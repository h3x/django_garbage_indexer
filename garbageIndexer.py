# views.py get "def" = all_views (url abgleich)
# views.py get .html = used_templates
# urls.py get "??" = used_views # re.compile()
# templates/xyz/* = all_templates

import os
import sys
import re
from functools import reduce

base_dir        = sys.argv[1]
clean_literals  = ['\n','\t']
skip_methods    = ["get","post"]
functions       = re.compile(r'^[\s]*def[^:(]*') # with params: r'^[\s]*def[^:]*'
html            = re.compile(r'[^/"\']*/?[^/"\']*.html')
view_keywoRds = ["as"]

def diff(all, sub):
    return ([ x for x in all if x not in sub])

def unique(input):
    return list(set(input))

def clean_string(text, literals=['\n','\t']):
    for lit in literals:
        text = text.replace(lit,'')
    return (text.lstrip())

def analyze_app(app):
    tmp_temps = []
    (all_views, used_templates) = analyze_views(app)
    used_views                  = analyze_urls(app)
    (all_templates, tmp_temps)              = analyze_templates(app)
    return ((all_views, used_views, all_templates, unique(used_templates + tmp_temps)))

def analyze_views(app):
    all_views = []
    used_templates = []
    with open(app + "/views.py" ) as view_file:
        for line in view_file:
            if("def " in line and "request" in line):
                fun_name = clean_string(line,['\n','\t',':'])
                #print(fun_name.replace("def ",""))
                fun_name = re.search(functions, fun_name).group().replace("def ","")
                if(fun_name not in skip_methods):
                    all_views.append(app.split('/')[-1] + "/" + fun_name)
            elif(".htm" in line and "return" in line):
                templ_name = clean_string(line)
                used_templates.append(re.search(html, templ_name).group().replace(base_dir,""))
    return ((unique(all_views), unique(used_templates)))

def analyze_urls(app):
    used_views = []
    with open(app + "/urls.py" ) as url_file:
        for line in url_file:
            if("path" in line and "(" in line):
                params = line.replace("path","").replace(")","").replace("(","").replace(".","/")
                view_name = params.split(',')[1]
                used_views.append(view_name.replace("/as_view","").replace(" ",""))
    return (unique(used_views))

def analyze_templates(app):
    all_templates = []
    used_ts = []
    for con in os.listdir(app + "/templates/"):
        # has safety subdir
        sub_dir = app + "/" + con
        if(os.path.isdir(app + "/templates/" + con)):
            sub_dir = app + "/templates/" + con            
        for temp in os.listdir(sub_dir):
            all_templates.append((app + "/" +  temp).replace(base_dir + "/", "").replace("/templates/",""))
            with open(sub_dir + "/" + temp ) as template_file:
                for line in template_file:
                    if(".html" in line):
                        line = line.replace(" ","").replace("\n","").replace("%","").replace("{","").replace("}","").replace('"',"'")
                        t_name = [ x for x in line.split("'") if x != ""][-1]
                        used_ts.append(t_name)
    return (unique(all_templates), unique(used_ts))

def valid_app(liste):
    tokens = ["urls.py", "views.py"]
    return(reduce(lambda a, b: a and b, [ token in liste for token in tokens ] ))

def get_apps(base_dir):
    app_names = []
    if(os.path.isdir(base_dir)):
        contents = [ base_dir + "/"+ soft_path for soft_path in os.listdir(base_dir)]
        dirs = [ dir_name for dir_name in contents if os.path.isdir(dir_name)]
        files = [ dir_name for dir_name in contents if os.path.isfile(dir_name)]
        for app in dirs:
            app_files = os.listdir(app)
            if(valid_app(app_files)):
                #'romotionCloud/parks'
                app_names.append(app)
    return (app_names)
# RUN!
all_temps = []
temps  = []
views = []
all_vs = []
apps = get_apps(base_dir)


for app in apps:
    (all_views, used_views, all_templates, used_templates) = analyze_app(app)
    all_temps += unique(all_templates)
    temps += unique(used_templates)
    all_vs += unique(all_views)
    views += unique(used_views)
        
print( diff(all_temps, temps) )
print( diff(all_views, used_views) )
#print(all_temps)    
